#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

mod raw {
	extern "C" {
		/// Get the length of the input buffer.
		pub fn input_buffer_len() -> usize;

		/// Read from the input buffer.
		pub fn input_buffer_read(ptr: *mut u8);

		/// Write to the output buffer.
		pub fn output_buffer_write(ptr: *const u8, len: usize);

		/// Flag the output buffer as an error message.
		pub fn raise();
	}
}

fn read_input() -> Vec<u8> {
	let len = unsafe { raw::input_buffer_len() };
	let mut buffer = Vec::with_capacity(len);
	unsafe { raw::input_buffer_read(buffer.as_mut_ptr()) };
	unsafe { buffer.set_len(len) };
	buffer
}

fn write_output(src: &[u8]) {
	unsafe { raw::output_buffer_write(src.as_ptr(), src.len()) }
}

fn raise() {
	unsafe { raw::raise() }
}

#[no_mangle]
pub extern "C" fn translate_luajit(include_runtime: bool) {
	fn inner(include_runtime: bool) -> Result<String, String> {
		let buffer = read_input();
		let module = wasm_ast::module::Module::try_from_data(&buffer).map_err(|e| e.to_string())?;

		let mut output = Vec::new();
		codegen_luajit::from_module_untyped(&module, &mut output).map_err(|e| e.to_string())?;

		String::from_utf8(output)
			.map(|s| {
				if include_runtime {
					format!(
						"local rt = (function()\n{}\nend)()\n{}",
						codegen_luajit::RUNTIME,
						s
					)
				} else {
					s
				}
			})
			.map_err(|e| e.to_string())
	}

	match inner(include_runtime) {
		Ok(output) => {
			write_output(output.as_bytes());
		}
		Err(error) => {
			write_output(error.as_bytes());
			raise();
		}
	}
}

#[no_mangle]
pub extern "C" fn translate_luau(include_runtime: bool) {
	fn inner(include_runtime: bool) -> Result<String, String> {
		let buffer = read_input();
		let module = wasm_ast::module::Module::try_from_data(&buffer).map_err(|e| e.to_string())?;

		let mut output = Vec::new();
		codegen_luau::from_module_untyped(&module, &mut output).map_err(|e| e.to_string())?;

		String::from_utf8(output)
			.map(|s| {
				if include_runtime {
					format!("{}\n{}", codegen_luau::RUNTIME, s)
				} else {
					s
				}
			})
			.map_err(|e| e.to_string())
	}

	match inner(include_runtime) {
		Ok(output) => {
			write_output(output.as_bytes());
		}
		Err(error) => {
			write_output(error.as_bytes());
			raise();
		}
	}
}
