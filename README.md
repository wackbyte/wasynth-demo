# Wasynth Demo

A demonstration of [Wasynth](https://github.com/Rerumu/Wasynth) that runs in a browser.

[Click here to see it in action!](https://wackbyte.gitlab.io/wasynth-demo)
